package com.kwan.springbootkwan.service;


public interface CsdnAutoReplyService {

    /**
     * 评论自己的文章
     *
     * @return
     */
    void commentSelf();
}